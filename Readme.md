Backup Docker Volumes with BorgBackup
=====================================

[![pipeline status](https://gitlab.com/elritsch/docker-volume-backup/badges/master/pipeline.svg)](https://gitlab.com/elritsch/docker-volume-backup/commits/master)

Use [BorgBackup](https://borgbackup.readthedocs.io/en/stable/) to create incremental backups of Docker volumes.

How To
------

### Method 1 - Use a password file
To create a backup to a remote SSH destination, run the following (replace `{YOUR_DOCKER_VOLUME}`, `{YOUR_CLEARTEXT_PASSWORD_FILE}`, `{YOUR_REMOTE_LOCATION}` and the optional parameter `[YOUR_BACKUP_NAME]` accordingly):
```bash
docker run \
    --rm \
    -v {YOUR_DOCKER_VOLUME}:/data/:ro \
    -v {YOUR_CLEARTEXT_PASSWORD_FILE}:/.repo-password:ro \
    -v ~/.ssh/:/ssh-config/:ro \
    -e BORG_REPO=ssh://{YOUR_REMOTE_LOCATION} \
    elritsch/docker-volume-backup:latest [YOUR_BACKUP_NAME]
```

Concrete example:
```bash
# Prepare an example volume
docker volume create myvolume

# Set the borg repo password
echo "secret_password" > ~/.secret-repo-password

# Launch the backup
docker run \
    --rm \
    -v myvolume:/data/:ro \
    -v ~/.secret-repo-password:/.repo-password:ro \
    -v ~/.ssh/:/ssh-config/:ro \
    -e BORG_REPO=ssh://yourname@example.com/~/myvolume-borg-backup \
    elritsch/docker-volume-backup:latest myvolume
```


### Method 2 - Use a password environment variable

Replace `-v {YOUR_CLEARTEXT_PASSWORD_FILE}:/.repo-password:ro` in the command described in method 1 above by `-e BORG_PASSPHRASE={YOUR_PASSWORD}`:
```bash
docker run \
    --rm \
    -v {YOUR_DOCKER_VOLUME}:/data/:ro \
    -v ~/.ssh/:/ssh-config/:ro \
    -e BORG_REPO=ssh://{YOUR_REMOTE_LOCATION} \
    -e BORG_PASSPHRASE={YOUR_PASSWORD} \
    elritsch/docker-volume-backup:latest [YOUR_BACKUP_NAME]
```


Tip
---

Note that BorgBackup uses [various environment variables](https://borgbackup.readthedocs.io/en/stable/usage/general.html#environment-variables) which can be set via `-e` arguments in the `docker run` command.


Docker Hub
----------

This Docker image is available on Docker Hub: https://hub.docker.com/r/elritsch/docker-volume-backup


Source
------

The source to create this Docker image is available on GitLab: https://gitlab.com/elritsch/docker-volume-backup
